from django.utils import timezone
from django.db import models
# from order.models import Order




class Food(models.Model):
    title = models.CharField(max_length=100)
    category = models.ForeignKey('FoodCategory', related_name='food_category', on_delete=models.SET_NULL, null=True)
    price = models.PositiveSmallIntegerField()
    description = models.TextField()

    def __str__(self):
        return self.title


class Table(models.Model):
    name_of_tables = models.CharField(max_length=100)
    is_free = models.BooleanField(default=True)

    def __str__(self):
        return self.name_of_tables


class Department(models.Model):
    name_of_departments = models.CharField(max_length=100)

    def __str__(self):
        return self.name_of_departments


class FoodCategory(models.Model):
    title = models.CharField(max_length=100)
    department = models.ForeignKey('Department', related_name='food_categories', on_delete=models.SET_NULL, null=True)

    def __str__(self):
        return self.title


class Order(models.Model):
    table = models.ForeignKey(Table, on_delete=models.SET_NULL, null=True)
    isitopen = models.BooleanField(default=True)
    food = models.ManyToManyField(Food, related_name='orders')
    date = models.DateTimeField(default=timezone.now)

    def __str__(self):
        return '{} + {} + {}'.format(self.table, self.isitopen, str(self.food.all().count()))

    def save(self, *args, **kwargs):
        _creating = False
        if not self.id:
            _creating = True
        super().save(*args, **kwargs)

        if _creating:
            self.table.is_free = False
            self.table.save()

        if not self.isitopen:
            self.table.is_free = True
            self.table.save()
