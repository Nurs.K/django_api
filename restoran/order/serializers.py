from rest_framework import serializers

from food.models import Food, Table
from food.serializers import FoodSerializer, TableSerializer
from food.models import Order


class OrderSerializer(serializers.ModelSerializer):
    table = serializers.PrimaryKeyRelatedField(
        queryset=Table.objects.all(),
    )
    table_name = TableSerializer(
        source='table',
        read_only=True,
    )
    food = serializers.SlugRelatedField(
        queryset=Food.objects.all(),
        many=True,
        slug_field='title',

    )
    food_name = FoodSerializer(
         source='food',
         read_only=True,
         many=True,

    )

    class Meta:
        model = Order
        fields = (
            'id',
            'table',
            'table_name',
            'food',
            'food_name',
            'isitopen',
            'date',
        )
