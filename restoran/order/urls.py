from django.urls import path
from rest_framework.urlpatterns import format_suffix_patterns
from .views import (
    OrderViewList,
    OrderCreateAPIView,
    OrderViewDetail,
    OrderViewDelete,
    OrderViewUpdate,
)

# API endpoints
urlpatterns = format_suffix_patterns([
    path('', OrderViewList.as_view(), name='order'),
    path('create/', OrderCreateAPIView.as_view(), name='order_create'),
    path('<int:id>/', OrderViewDetail.as_view(), name='order_details'),
    path('<int:id>/delete/', OrderViewDelete.as_view(), name='order_delete'),
    path('<int:id>/update/', OrderViewUpdate.as_view(), name='order_update'),
])